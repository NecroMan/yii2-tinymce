<?php
namespace milano\tinymce\fm;

use yii\base\BaseObject;
use yii\web\JsExpression;

/**
 * Abstract FileManager to use with TinyMce.
 * For example see elFinder extension.
 */
abstract class FileManager extends BaseObject
{

    /**
     * Initialize FileManager component, registers required JS
     */
    public function init()
    {

    }

    /**
     * @return JsExpression JavaScript callback function
     */
    abstract public function getFileBrowserCallback();

    abstract public function registerAsset();
}
