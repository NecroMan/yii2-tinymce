<?php
namespace milano\tinymce;

class TinyMceAsset extends \yii\web\AssetBundle
{

    public $sourcePath = '@bower/tinymce';
    public $js = [
        'tinymce.min.js',
        'jquery.tinymce.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}